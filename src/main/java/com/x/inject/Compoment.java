package com.x.inject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @program: comx
 * @description: @Compoment注解
 *                          放在类上，适用该注解自动管理bean和实例
 * @author: wangxilong
 * @create: 2019-10-09 14:29
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Compoment {
}
